import datetime

girls = [{
    'name': 'Лиза Боярчук',
    'instagram': 'prostokapec',
    'photo': '/resources/girls/prostokapec.jpg'
}, {
    'name': 'Соня Краснокутская',
    'instagram': 'kkrasnokutska',
    'photo': '/resources/girls/kkrasnokutska.jpg'
}, {
    'name': 'Ева Степанова',
    'instagram': 'stepaschaa',
    'photo': '/resources/girls/stepaschaa.jpg'
}, {
    'name': 'Неля Блинова',
    'instagram': '_neonelka_',
    'photo': '/resources/girls/_neonelka_.jpg'
}, {
    'name': 'Арина Кливак',
    'instagram': 'klivaaak',
    'photo': '/resources/girls/klivaaak.jpg'
}, {
    'name': 'Саша Саносян',
    'instagram': 'alexandrasanosyan',
    'photo': '/resources/girls/alexandrasanosyan.jpg'
}, {
    'name': 'Ева Лукашина',
    'instagram': 'evalukashyna',
    'photo': '/resources/girls/evalukashyna.jpg'
}, {
    'name': 'Инна Глущенко',
    'instagram': 'inna_glushchenko',
    'photo': '/resources/girls/inna_glushchenko.jpg'
}, {
    'name': 'Юля Тарасенко',
    'instagram': 'cloudlet_jt',
    'photo': '/resources/girls/cloudlet_jt.jpg'
}, {
    'name': 'Соня Ильина',
    'instagram': 'sophie_ilina',
    'photo': '/resources/girls/sophie_ilina.jpg'
}, {
    'name': 'Таня Супрун',
    'instagram': 'spooky_pu',
    'photo': '/resources/girls/spooky_pu.jpg'
}, {
    'name': 'Алина Гарбузова',
    'instagram': 'ichbinolina',
    'photo': '/resources/girls/ichbinolina.jpg'
}]

contests = [{
    'first_girl_id': 1,
    'second_girl_id': 2,
    'begin': datetime.datetime(2019, 6, 7),
    'end': datetime.datetime(2019, 6, 10),
    'first_girl_win_chance': 0.5
}, {
    'first_girl_id': 3,
    'second_girl_id': 4,
    'begin': datetime.datetime(2019, 6, 7),
    'end': datetime.datetime(2019, 6, 10),
    'first_girl_win_chance': 0.5
}, {
    'first_girl_id': 5,
    'second_girl_id': 6,
    'begin': datetime.datetime(2019, 6, 10),
    'end': datetime.datetime(2019, 6, 13),
    'first_girl_win_chance': 0.5
}, {
    'first_girl_id': 7,
    'second_girl_id': 8,
    'begin': datetime.datetime(2019, 6, 10),
    'end': datetime.datetime(2019, 6, 13),
    'first_girl_win_chance': 0.5
}]

users=[]
